from json import load
from pathlib import Path


from flask import Blueprint, abort, jsonify
from fuzzywuzzy import process


gods = Blueprint('gods', __name__)


@gods.route('/gods/info/<god>', methods=['GET'])
def get_god_info(god):
    """Return a dict containing information about a Greek god."""

    with open(Path('../data', 'gods.json'), 'r') as f:
        data = load(f)

    choices = [key for key in data]
    best_choice = process.extract(god, choices, limit=1)
    if best_choice[0][1] < 80:
        abort(400)
    else:
        god = best_choice[0][0]

    return jsonify({"name": god,
                    "greek": data[god]["greek"],
                    "deity": data[god]["deity"],
                    "description": data[god]["description"]})
