from flask import Flask
from gods import gods
from iliad import iliad
from labyrinth import labyrinth
from trojan import trojan


app = Flask(__name__)

app.register_blueprint(gods)
app.register_blueprint(trojan)
app.register_blueprint(labyrinth)
app.register_blueprint(iliad)

app.run()
