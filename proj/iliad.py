import random
from json import load
from pathlib import Path


from flask import Blueprint, jsonify


iliad = Blueprint('iliad', __name__)


@iliad.route('/iliad/quote/random', methods=['GET'])
def iliad_quote():
    """Return a random quote by Homer from The Iliad."""

    with open(Path('../data', 'iliad.json'), 'r') as f:
        data = load(f)

    quote = random.choice(data['quotes'])

    return jsonify({'quote': quote})
