import random
import string
from json import dump, load
from pathlib import Path


from flask import Blueprint, jsonify


labyrinth = Blueprint('labyrinth', __name__)


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


@labyrinth.route('/labyrinth/start', methods=['GET'])
def start():
    """Creates and returns 3 paths, one of them being correct."""

    with open(Path('../data', 'labyrinth.json'), 'r') as f:
        data = load(f)

    first = id_generator()
    second = id_generator()
    third = id_generator()

    return_paths = [first, second, third]
    random.shuffle(return_paths)

    save_paths = {
        first: True,
        second: False,
        third: False
    }

    data['paths'].append(save_paths)

    with open(Path('../data', 'labyrinth.json'), 'w') as f:
        dump(data, f, indent=4)

    return jsonify({'paths': [i for i in return_paths]})


@labyrinth.route('/labyrinth/<route>', methods=['GET'])
def proceed(route):
    """Checks if a picked route is the right one."""

    with open(Path('../data', 'labyrinth.json'), 'r') as f:
        data = load(f)

    choice = {'path': False}

    for i in data['paths']:
        for key, value in i.items():
            if key == route and value is True:
                del data['paths'][data['paths'].index(i)]
                choice = {'path': True}
            elif key == route and value is not True:
                del data['paths'][data['paths'].index(i)]

    with open(Path('../data', 'labyrinth.json'), 'w') as f:
        dump(data, f, indent=4)

    return jsonify(choice)
