import random
from json import dump, load, loads
from pathlib import Path


from flask import Blueprint, abort, jsonify, request


trojan = Blueprint('trojan', __name__)


def random_solider():
    """Return name of a random Greek solider."""

    with open(Path('../data', 'names.json'), 'r') as f:
        data = load(f)

    return random.choice(data['names'])


@trojan.route('/trojan/enter', methods=['PUT'])
def trojan_enter():
    """Load up a solider with a randomly generated Greek name and a message to his family."""

    message = loads(request.data.decode())

    with open(Path('../data', 'trojan.json'), 'r') as f:
        data = load(f)

    solider = random_solider()

    if message.get('message', False) is not False:
        data[solider] = {'message': message['message']}
    else:
        abort(400)

    with open(Path('../data', 'trojan.json'), 'w') as f:
        dump(data, f, indent=4)

    return jsonify({solider: {'message': message['message']}})


@trojan.route('/trojan/open', methods=['GET'])
def trojan_open():
    """Open up the Trojan Horse in Troy territory."""

    with open(Path('../data', 'trojan.json'), 'r') as f:
        data = load(f)

    data = {}

    with open(Path('../data', 'trojan.json'), 'w') as f:
        dump(data, f, indent=4)

    return 'Koutávia ékplixi!'
