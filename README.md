# Documentation

This is documentation for a total of 6 endpoints our team has created in the 2nd Code Jam of the Python Discord.
Requests made below are examples.

---

`GET /gods/info/<god>`

Return a dict containing information about a Greek god.

```python
requests.get('https://127.0.0.1/gods/info/Zeus')
```

will return:

```json
{
	"deity": "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c8/Jupiter_Smyrna_Louvre_Ma13.jpg/160px-Jupiter_Smyrna_Louvre_Ma13.jpg",
	"description": "King of the gods, ruler of Mount Olympus, and god of the sky, weather, thunder, lightning, law, order, and justice. He is the youngest son of Cronus and Rhea. He overthrew Cronus and gained the sovereignty of heaven for himself. In art he is depicted as a regal, mature man with a sturdy figure and dark beard. His usual attributes are the royal scepter and the lightning bolt. His sacred animals include the eagle and the bull. His Roman counterpart is Jupiter, also known as Jove.",
	"greek": "Ζεύς",
	"name": "Zeus"
}
```

---

`PUT /trojan/enter` 

Load up a solider with a randomly generated Greek name and a message to his family.

```python
requests.put('http://127.0.0.1:5000/trojan/enter', data=json.dumps({'message': 'Hello World!'}))
```

will return:

```json
{"Daedalus":{"message":"Hello World!"}}
```

---

`GET /trojan/open`

Open up the Trojan Horse in Troy territory.

```python
requests.get('https://127.0.0.1/trojan/open')
```

will return:

```
'Koutávia ékplixi!'
```

---

`GET /labyrinth/start`

Creates and returns 3 paths, one of them being correct.

```python
requests.get('http://127.0.0.1:5000/labyrinth/start')
```

will return:

```json
{
	"paths": [
		"UXIEWS",
		"HBMQXV",
		"F6N6RI"
	]
}
```

---

`GET /labyrinth/<route>`

Checks if a picked route is the right one.

```python
requests.get('http://127.0.0.1:5000/labyrinth/UXIEWS')
```

will return:

```json
{
	"path": false
}
```

---

`GET /iliad/quote/random`

Return a random quote by Homer from The Iliad.

```python
requests.get('http://127.0.0.1:5000/iliad/quote/random')
```

will return:

```json
{
	"quote": "We men are wretched things."
}
```
